package main

import "fmt"
import "net/http"
import "html/template"

func list(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "List buku telepon")
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		var menu = map[string]string{
			"appname": "Bupon",
			"home":    "Beranda",
			"list":    "Daftar Nomor",
			"setting": "Pengaturan",
		}

		var t, err = template.ParseFiles("static/html/template.html")

		if err != nil {
			fmt.Println(err.Error())
		}

		t.Execute(w, menu)

	})

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	http.HandleFunc("/list", list)

	fmt.Println("bupon running at http://localhost:8080/")
	http.ListenAndServe(":8080", nil)

}
